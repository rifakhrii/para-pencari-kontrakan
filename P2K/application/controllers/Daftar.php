<?php 

class Daftar extends CI_Controller
{
	public function __construct() {
		parent::__construct();
		$this-> load ->model('Daftar_model');
	}
	public function index()
	{
		$this-> load -> model('Daftar_model');
		$this-> load -> database();
		$data['judul']='Daftar Kost';
		$data['kost']= $this -> Daftar_model -> getAllKost();
		$this-> load -> view('templates/header',$data);
		$this-> load -> view('Daftar/index');
		$this-> load -> view('templates/footer');
	}
	public function user(){
		$this-> load -> model('Daftar_model');
		$this-> load -> database();
		$data['judul']='Daftar Kost';
		$data['kost']= $this -> Daftar_model -> getAllKost();
		$this-> load -> view('templates/header',$data);
		$this-> load -> view('Daftar/user');
		$this-> load -> view('templates/footer');
	}
	public function tambah()
	{
		$this -> load -> model('Daftar_model');
		$data['judul'] = 'Form Tambah Data';
		$this -> load -> library('form_validation');
		$this -> form_validation -> set_rules('id','Id','required|max_length[4]');
		$this -> form_validation -> set_rules('nama','Nama','required');
		$this -> form_validation -> set_rules('lokasi','Lokasi','required');
		$this -> form_validation -> set_rules('pemilik','Pemilik','required');
		$this -> form_validation -> set_rules('nohp','Nomor Handphone','required|numeric|min_length[11]');
		$this -> form_validation -> set_rules('spesifikasi','spesifikasi','required');
		if( $this -> form_validation -> run() == FALSE){
			$this -> load -> view('templates/header', $data);
			$this -> load -> view('Daftar/tambah');
			$this -> load -> view('templates/footer');
		} else {
		$this -> Daftar_model ->tambahData();
		$this -> session -> set_flashdata('flash','Di Tambahkan');
		redirect('Daftar');
	}
	}
	public function hapus($id){
		$this -> load -> model('Daftar_model');
		$this -> Daftar_model -> hapusData($id);
		$this -> session -> set_flashdata('flash','Di Hapus');
		redirect('Daftar');
	}
}
?>