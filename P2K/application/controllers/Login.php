<?php 

class Login extends CI_Controller
{	
	public function index()
	{
		$this -> load -> model('Login_model');
		$this -> load -> library('form_validation');
		$this -> form_validation -> set_rules('username','Username','required|min_length[5]|max_length[11]');
		$this -> form_validation -> set_rules('password','Password','required|min_length[6]|max_length[11]');
		$data['judul']="Login Page";
		if ($this->form_validation->run() == FALSE)
                {
                    $this -> load -> view('templates/header',$data);
					$this -> load -> view('login/index');
					$this -> load -> view('templates/footer'); 
                }
                else
                {
                    $this -> Login_model -> login();
                    $this -> session -> set_flashdata('login','login');
                    redirect('Daftar');  
                }
	}
}
?>