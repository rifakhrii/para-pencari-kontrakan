<div class="container " align="center">
  <div class="container text-center">
     <div class="row">
      <div class="col">
      <img src="img/kamar.png" style="position:fixed; top:1; left:0; min-width:100%; min-height:100%;">
     </div>
  </div>
  <div class="row mt-5">
    <div class="col-md-5 col-ml-4">
      <div class="card text-white bg-warning text-center">
        <div class="card-header">
          <h2 class="text-light" >Page Login</h2>
          <img src="img/kost.png" width="80" class="img-responsive" >
            <small><?php echo validation_errors(); ?></small>
            <?php if($this -> session -> flashdata('message')): ?>
              <div class="row mt-3">
                <div class="col">
                  <div class="alert alert-info alert-dismissible fade show" role="alert">
                    <?php echo $this -> session -> flashdata('message'); ?>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                </div> 
              </div>
              <?php endif; ?>
        </div>
        <div class="card-body">
         <form action="" method="post">
          <div class="form-group ">
            <label for="username"><h4>Username</h4></label><br>
            <input type="text" name = "username" class="form-control" id="username" placeholder="Username" required="required">
          </div>
          <div class="form-group">
            <label for="password"><h4>Password</h4></label><br>
            <input type="password" name = "password" class="form-control" id="password" placeholder="Password" required="required">
          </div>
          <button type="submit" name="login" class="btn btn-primary btn-block"><h6 class="text-light">My Account</h6></button>
      </form>
        </div>
      </div>      
    </div>
    <div class="col-md-7">
     <div class="alert alert-primary" role="alert">
        <h4 class="alert-heading">Login</h4>
        <p>Masuk sebagai <strong>"Admin" </strong>anda dapat menambah dan menghapus data kontrakan</p>
        <p>Login sesuai dengan akun anda</p>
        <hr>
        <p class="mb-0"><strong>Team IT P2K</strong></p><br>
        <img src="img/P2K2.png" width="200">
    </div>
  </div>
</div>
