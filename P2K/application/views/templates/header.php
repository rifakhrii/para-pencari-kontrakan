<!doctype html>
<html lang="en">
  <head>

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- My Css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/style.css">
    <title>  <?php	echo $judul; ?></title>
  </head>
  <body>
<nav class="navbar navbar-expand-lg navbar-dark bg-success">
  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
    <div class="navbar-nav">
      <a class="text-light nav-item nav-link" href="<?php echo base_url(); ?>"><strong><h5>Para Pencari Kost</strong></h5></a>
      <a class="text-dark nav-item nav-link" href="<?php echo base_url(); ?>Daftar/user"><h5>Daftar</h5></a>
      <a class="text-dark nav-item nav-link" href="<?php echo base_url(); ?>Login"><h5>Login</h5></a>
      <a class="text-dark nav-item nav-link" href="<?php echo base_url(); ?>About"><h5>About</h5></a>
    </div>
  </div>
</nav>
