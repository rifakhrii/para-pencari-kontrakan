<div class="container text-center">
   <div class="row">
    <div class="col">
    <img src="img/kamar.png" style="position:fixed; top:1; left:0; min-width:100%; min-height:100%;">
   </div>
</div>
</div><section id="Keunggulan" class="Keunggulan">
<div class="container text-center">
	<div class="row mt-5">
		<div class="col">
		    <img src="img/kost.png" width="30%">
		    <img src="img/P2K2.png" width="55%">
		</div>
	</div>
	<div class="row mt-5 mb-4">
		<div class="col">
			<h3>Keunggulan P2K</h3>
		</div>
	</div>
	<div class="row mb-4">
		<div class="col">
			<img src="img/money.png" width="80">
			<h5>Harga Terjangkau</h5>
		</div>
		<div class="col">
			<img src="img/cepat.png" width="80">
			<h5>Mudah dan Cepat</h5>
		</div>
		<div class="col">
			<img src="img/terpercaya3.png" width="80">
			<h5>Terpercaya</h5>
		</div>
	</div>
</div>
</section>
<div class="container">
	<div class="row pt-4 mb-4">
      <div class="col-md-4">
      	<div class="card text-white bg-primary text-center">
			<div class="card-body">
			<h5 class="card-title">My Office</h5>
			<p class="card-text">Kirim Kami Pesan atau Kunjungi Kantor Kami untuk segala Keluhan Anda</p>
			</div>
		</div>
		<ul class="list-group mb-4">
			<li class="list-group-item"><h3>Location</h3></li>
			<li class="list-group-item">Jln. D.I Panjaitan</li>
			<li class="list-group-item">Purwokerto, Cental Java</li>
			<li class="list-group-item">Phone: 082134521234 (Admin)</li>
			<li class="list-group-item">Email: p2kmanagement@gmail.com</li>
		</ul>
      </div>
      <div class="col-md-4">
		  <div class="alert alert-info" role="alert">
			  <h4 class="alert-heading">Pemberitahuan!</h4>
			  <hr>
			  <p>Ingin memasang iklan info kontrakan anda?</p>
			  <p class="mb-0">Silahkan hubungi admin, Terima kasih</p>
			  <hr>
			  <p>~ Admin</p>
			  <div class="spinner-grow text-primary" role="status">
				  <span class="sr-only">Loading...</span>
				</div>
				<div class="spinner-grow text-primary" role="status">
				  <span class="sr-only">Loading...</span>
				</div>
				<div class="spinner-grow text-primary" role="status">
				<span class="sr-only">Loading...</span>
				</div>
		</div>
      </div>
      <div class="col-md-4">
		  <div class="alert alert-warning" role="alert">
			  <h4 class="alert-heading">Promo!</h4>
			  <hr>
			  <p>Dapatkan promo menarik untuk setiap </p>
			  <p class="mb-0">yang baru memasang iklan</p>
			  <hr>
			  <p>~ Admin</p>
			  <div class="spinner-grow text-warning" role="status">
				  <span class="sr-only">Loading...</span>
				</div>
				<div class="spinner-grow text-warning" role="status">
				  <span class="sr-only">Loading...</span>
				</div>
				<div class="spinner-grow text-warning" role="status">
				<span class="sr-only">Loading...</span>
				</div>
			</div>
      	</div>  
     </div>  
</div>
      		