<div class="card mb-3">
  <div class="container mt-3 text-center">
  <img src="img/kost.png" width="15%">
      <div class="card-body">
        <h4 class="card-title"><strong>Penggagas</strong></h4>
        <p class="card-text">Di buat oleh mahasiswa IT Telkom Purwoketo dalam rangka menyelesaikan permasalahan di masyarakat oleh sulitnya mencari kost dan di harapkan dapat meningkatkan perekonomian masyarakat</p>
        <p class="card-text"><small class="text-muted">Team P2K</small></p>
      </div>
    </div>
</div>
<div class="container text-center">
<div class="row mt-3 mb-3">
    <div class="col">
        <h5>Dekat dengan: </h5>
    </div>
    <div class="row">
    <div class="col">
      <img src="img/telkom.png" width="100">
      <h5>IT Telkom Purwokerto</h5>
    </div>
    <div class="col">
      <img src="img/unsoed.png" width="90">
      <h5>Universitas Jenderal Soedirman</h5>
    </div>
    <div class="col">
      <img src="img/IAIN.png" width="140">
      <h5>IAIN Purwokerto</h5>
    </div>
    <div class="col">
      <img src="img/ump.png" width="95">
      <h5>Universitas Muhammadiyah Purwokerto</h5>
    </div>
  </div>
</div>
</div>