<div class="container">
	<div class="row mt-3">
		<div class="col md-6">
			<h3>List Daftar Kost </h3>
		</div>
	</div>
  <div class="row mt-3">
    <div class="col md-6">
      <table class="table" border="2">
  <thead>
    <tr>
      <th scope="col">Nama</th>
      <th scope="col">Alamat</th>
      <th scope="col">No HP / WA</th>
      <th scope="col">Spesifikasi</th>
      <th scope="col">Pemilik</th>
    </tr>
  </thead>
  <?php foreach($kost as $ks): ?>
  <tbody>
    <tr>
      <td><?php echo $ks['Nama'] ?></td>
      <td><?php echo $ks['Lokasi']; ?></td>
      <td><?php echo $ks['NoHp']; ?></td>
      <td><?php echo $ks['Spesifikasi']; ?></td>
      <td><?php echo $ks['Pemilik']; ?></td>
    </tr>
  </tbody>
  <?php endforeach; ?>
    </div>
  </div>
  <div class="row">
    <div class="col">
      <div class="alert alert-dismissible alert-warning">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <h4 class="alert-heading">Pemberitahuan</h4>
        <p class="mb-0"><strong>Transaksi</strong> bisa langsung <strong>menghubungi kontak </strong>yang tercantum</p>
        <div class="spinner-grow text-danger" role="status">
          <span class="sr-only">Loading...</span>
        </div>
        <div class="spinner-grow text-warning" role="status">
          <span class="sr-only">Loading...</span>
        </div>
        <div class="spinner-grow text-primary" role="status">
          <span class="sr-only">Loading...</span>
        </div>
      </div>
    </div>
  </div>
</div>