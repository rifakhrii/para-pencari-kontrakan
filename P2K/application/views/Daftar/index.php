<div class="container">
	<div class="row mt-3">
		<div class="col md-6">
			<a href="<?php echo base_url(); ?>Daftar/tambah" class="badge badge-primary"> Tambah Data Kost </a>
      <a href="<?php echo base_url(); ?>Login" class="badge badge-danger"> LogOut </a>
		</div>
	</div>
	<div class="row mt-3">
		<div class="col md-6">
			<h3>List Daftar Kost </h3>
		</div>
	</div>
	<?php if($this -> session -> flashdata('flash')): ?>
	<div class="row mt-3">
		<div class="col-md-6">
			<div class="alert alert-success alert-dismissible fade show" role="alert">
			  Data<strong> berhasil</strong> <?php echo $this -> session -> flashdata('flash'); ?>
			  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
			    <span aria-hidden="true">&times;</span>
			  </button>
			</div>
		</div> 
	</div>
	<?php endif; ?>
    <?php if($this -> session -> flashdata('login')): ?>
  <div class="row mt-3">
    <div class="col-md-6">
      <div class="alert alert-success alert-dismissible fade show" role="alert">
        Anda<strong> berhasil</strong> <?php echo $this -> session -> flashdata('login'); ?>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    </div> 
  </div>
  <?php endif; ?>
<div class="alert alert-success" role="alert">
  ~~ Anda dapat <strong>menambah </strong>dan <strong>menghapus </strong>data kontrakan ~~
</div>
<div class="row mt-3">
  <div class="col">
    <table class="table" border="2">
  <thead>
    <tr>
      <th scope="col">ID</th>
      <th scope="col">Nama</th>
      <th scope="col">Alamat</th>
      <th scope="col">No HP / WA</th>
      <th scope="col">Spesifikasi</th>
      <th scope="col">Pemilik</th>
      <th scope="col">Hapus</th>
    </tr>
  </thead>
  <?php foreach($kost as $ks): ?>
  <tbody>
    <tr>
      <td><?php echo $ks['id'] ?></td>
      <td><?php echo $ks['Nama'] ?></td>
      <td><?php echo $ks['Lokasi']; ?></td>
      <td><?php echo $ks['NoHp']; ?></td>
      <td><?php echo $ks['Spesifikasi']; ?></td>
      <td><?php echo $ks['Pemilik']; ?></td>
      <td><a href="<?php base_url(); ?>Daftar/hapus/<?php echo $ks['id']; ?>" class="badge badge-danger float-right" onclick ="return confirm('Yakin?');">Hapus</a></td>
    </tr>
  </tbody>
  <?php endforeach; ?>
  </div>
</div>
</div>
