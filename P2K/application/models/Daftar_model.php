<?php 
class Daftar_model extends CI_Model
{
	
	public function getAllKost()
	{
		$query = $this->db->get('sewa'); 
		return $query-> result_array();
	}
	public function tambahData()
	{
		$data = [
			"id" => $this -> input -> post('id'),
			"nama" => $this -> input -> post('nama'),
			"lokasi" => $this -> input -> post('lokasi'),
			"pemilik" => $this -> input -> post('pemilik'),
			"nohp" => $this -> input -> post('nohp'),
			"spesifikasi" => $this -> input -> post('spesifikasi'),
			];
		$this->db->insert('sewa', $data);		
	}

	public function hapusData($id){		
		$this->db->delete('sewa', array('id' => $id));
	}

}
?>